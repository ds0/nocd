from flask import Flask, render_template, request, jsonify
import os
import itertools
from random import randint
app = Flask(__name__)

deck = {}
board = {}

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/sync')
def sync():
    return jsonify(deck=deck, board=board)

@app.route('/place_card')
def place_card():
    card_id = request.args.get('card_id', 0, type=int)
    if card_id in deck:
        board[card_id] = deck[card_id]
        del deck[card_id]
    x = request.args.get('x')
    y = request.args.get('y')
    board[card_id]["x"] = x
    board[card_id]["y"] = y
    return jsonify(deck=deck, board=board)

@app.route('/place_random_card')
def place_random_card():
    if len(deck) > 0:
        card_id = 0
        if len(deck) == 1:
            for card in deck:
                card_id = card
        else:
            list_deck = []
            for card_id in deck:
                list_deck.append(card_id)
            list_card = randint(0, len(list_deck)-1)
            card_id = list_deck[list_card]
        if card_id in deck:
            board[card_id] = deck[card_id]
            del deck[card_id]
        x = request.args.get('x')
        y = request.args.get('y')
        board[card_id]["x"] = x
        board[card_id]["y"] = y
        return jsonify(deck=deck, board=board)
    else:
        return jsonify(deck=deck, board=board)

@app.route('/return_card')
def return_card():
    card_id = request.args.get('card_id', 0, type=int)
    if card_id in board:
        deck[card_id] = board[card_id]
        del board[card_id]
    return jsonify(deck=deck, board=board)

def init_deck():
    for i in range(78):
        card = {
            "x": -1,
            "y": -1
        }
        deck[i] = card

if __name__ == '__main__':
    init_deck()
    app.run(host='0.0.0.0', debug=True)

