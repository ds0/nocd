// load metadata for cards (names, maybe descriptions later)
var deck_meta = (function() {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/static/deck_meta.json',
		'dataType': 'json',
		'success': function(data) {
			json = data;
		}
	});
	return json;
})();

$(document).ready(function() {	
	var mouseX = 0;
	var mouseY = 0;
	var boardX = 0; // position of mouse relative to board's top-left corner
	var boardY = 0;
	var cardX = 0; // position of mouse relative to selected card
	var cardY = 0;
	var holding_card = false;
	var held_card = null; // stores ID of currently held card
	var returned_card = null;
	var card_from_board = false; // true when a card on the board is clicked
	var empty_deck = false;
	var state = null; // stores which cards are in deck and which are on board
	var refresh_rate = 500; // measured in ms

	$('#deck').click(function() {
		if(holding_card) {
			if (held_card == null) {
				toggle_holding_card();
			} else {
				$('#'+held_card).remove();
				$.getJSON($SCRIPT_ROOT + '/return_card',
				{ card_id: held_card },
				function(data) {
					state = data;
					sync();
				});
				held_card = null;
				toggle_holding_card();
			}
		} else if (!empty_deck) {
			cardX = parseInt(mouseX - $(this).offset().left);
			cardY = parseInt(mouseY - $(this).offset().top);
			toggle_holding_card();
		}
	});

	// a card, once held, follows the mouse.
	$(document).mousemove(function(e) {
		mouseX = parseInt(e.pageX);
		mouseY = parseInt(e.pageY);
		boardX = parseInt(mouseX - cardX - $('#board').offset().left);
		boardY = parseInt(mouseY - cardY - $('#board').offset().top);
		if(holding_card) {
			$('#held_card').css({ left: mouseX - cardX, top: mouseY - cardY });
		}
	});

	$('#board').click(function() {
		// place a card if it's held.
		if(holding_card) {
			var cardW = parseInt($('#held_card').width());
			var cardH = parseInt($('#held_card').height());
			var boardW = parseInt($('#board').width());
			var boardH = parseInt($('#board').height());
			var placeX = Math.min(boardX, boardW - cardW);
			var placeY = Math.min(boardY, boardH - cardH);
			if(placeX < 0) {
				placeX = 0;
			}
			if(placeY < 0) {
				placeY = 0;
			}
			var requestX = placeX / parseInt($('#board').width());
			var requestY = placeY / parseInt($('#board').height());
			if(held_card == null) {
				//ajax call to get a random card from the deck
				$.getJSON($SCRIPT_ROOT + '/place_random_card',
				{ x: requestX, y: requestY },
				function(data) {
					state = data;
					sync();
					resize_cards();
					toggle_holding_card();
				});
			} else if(!card_from_board) {
				// held_card gets cleared before the ajax call finishes but is still useful for drawing.
				if($('.held').length) {
					$('#'+held_card).parent().append($('#'+held_card));
					$('#'+held_card).css('left', placeX+'px');
					$('#'+held_card).css('top', placeY+'px');
					$('#'+held_card).css('display', 'block');
					$('#'+held_card).removeClass('held');
					// make sure the repositioned card appears above the others
					$('.card').each(function() {
						$(this).css('z-index', 0);
					});
					$('#'+held_card).css('z-index', 1);
				} else {
					$('#board').append("<div class='card' id='"+held_card+"' style='left: "+placeX+"px; top:"+placeY+"px'><img class='card_img' src='static/img/"+held_card+".jpg'></img></div>");
				}
				resize_cards();
				$.getJSON($SCRIPT_ROOT + '/place_card',
				{ x: requestX, y: requestY, card_id: held_card },
				function(data) {
					state = data;
					sync();
				});
				toggle_holding_card();
				protect_card(); // avoid the immediate replacement of the last held card
			} else {
				card_from_board = false;
			}
		}
	});
	
	$('#board').on('click', '.card', function() {
		// get the mouse's position relative to the card so it snaps to it when following
		cardX = parseInt(mouseX - $(this).offset().left);
		cardY = parseInt(mouseY - $(this).offset().top);
		if(held_card == null && !holding_card) {
			held_card = $(this).attr('id');
			$(this).css("display", "none");
			$(this).addClass("held");
			$(this).addClass("last_held");
			toggle_holding_card();
			card_from_board = true;
		}
	});

	$('#card_menu').on('click', '.menu_card', function() {
		if(held_card == null && !holding_card) {
			held_card = $(this).attr('id').split('_')[1];
			toggle_holding_card();
		}
	});

	function toggle_holding_card() {
		holding_card = !holding_card;
		if(holding_card) {
			if(held_card != null) {
				$('#held_card').html("<img class='card_img' src='static/img/"+held_card+".jpg'></img>");
			}
			$('#held_card').css('display', 'block');
			$('#held_card').css({ left: mouseX - cardX, top: mouseY - cardY });
		} else {
			held_card = null;
			$('#held_card').html('');
			$('#held_card').css('display', 'none');
		}
	}

	function review_deck() {
		if(Object.keys(state['deck']).length == 0) {
			$('#deck').css('border', '1px dotted #ff8');
			$('#deck').css('background-color', '#304');
			empty_deck = true;
		} else {
			$('#deck').css('border', '4px solid white');
			$('#deck').css('background-color', '#627');
			$('#deck').css('border-radius', '4px');
			empty_deck = false;
		}
	}

	function set_card_list() {
		$('#card_menu').html('');
		for(var card_id in state['deck']) {
			$('#card_menu').append("<div class='menu_card' id='menu_"+card_id+"'>"+deck_meta[card_id]['name']+"</div>");
		}
	}

	function sync() {
		// check if deck should be shown as emptied out
		review_deck();
		set_card_list();
		cards_on_board = [];
		$(".card").each(function() {
			cards_on_board.push($(this).attr('id'));
		});
		for(var card_id in state['board']) {
			// place any new card, but don't redraw any held card
			if(cards_on_board.indexOf(card_id) == -1 && card_id != held_card) {
				var placeX = parseInt(state['board'][card_id]['x'] * parseInt($('#board').width()));
				var placeY = parseInt(state['board'][card_id]['y'] * parseInt($('#board').height()));
				$('#board').append("<div class='card' id='"+card_id+"' style='left: "+placeX+"px; top:"+placeY+"px; z-index: 1;'><img class='card_img' src='static/img/"+card_id+".jpg'></img></div>");
			}
		}
		// remove any returned cards
		for(var card_id in state['deck']) {
			if(cards_on_board.indexOf(card_id) > -1) {
				$('#'+card_id).remove();
			}
		}
		replace_cards();
		resize_cards();
	}

	function convert_placement(x, y) {
		var placeX = parseInt(x * parseInt($('#board').width()));
		var placeY = parseInt(y * parseInt($('#board').height()));
		return [placeX, placeY];
	}

	function replace_cards() {
		$('.card').not('.last_held').each(function() {
			$(this).parent().append($(this));
			var card_id = $(this).attr('id');
			var placeX = parseInt(state['board'][card_id]['x'] * parseInt($('#board').width()));
			var placeY = parseInt(state['board'][card_id]['y'] * parseInt($('#board').height()));
			if( (placeX+'px' != $('#'+card_id).css('left')) 
			|| (placeY+'px' != $('#'+card_id).css('top')) ) {
				$('#'+card_id).css({
					top: placeY,
					left: placeX
				});
				$('#'+card_id).css('z-index', '1');
			}
		});
	}

	function protect_card() {
		setTimeout(function() {
			$('.card').removeClass('last_held');
		}, refresh_rate);
	}

	function resize_cards() {
		var cardH = parseInt(0.3 * parseInt($('#board').height()));
		var cardW = parseInt(cardH * 37 / 65);
		$('#held_card, #deck, .card').css('width', cardW+'px');
		$('#held_card, #deck, .card').css('height', cardH+'px');
	}
	
	$(window).resize(function(event) {
		resize_cards();
		replace_cards();
	});

	resize_cards();
	$.getJSON($SCRIPT_ROOT + '/sync', {},
	function(data) {
		state = data;
		sync();
		resize_cards();
	});
		
	window.setInterval(function() {
		$.getJSON($SCRIPT_ROOT + '/sync', {},
		function(data) {
			state = data;
			sync();
		});
	}, refresh_rate);
});

