import json

deck = {}
card_id = 0
with open("major_arcana.txt", "r") as major_arcana:
    for line in major_arcana:
        card = {
            "name": line.strip("\n")
        }
        deck[card_id] = card
        card_id += 1
suits = ["Cups", "Pentacles", "Swords", "Wands"]
values = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "King", "Knight", "Page", "Queen"]
for suit in suits:
    for value in values:
        card = {
            "name": value + " of " + suit
        }
        deck[card_id] = card
        card_id += 1

with open("static/deck_meta.json", "w") as deck_meta:
    json.dump(deck, deck_meta)
